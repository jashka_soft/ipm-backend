<?php

Route::group(['prefix' => 'auth'], function () {
    Route::post('signup', 'AuthenticateController@signUp');
    Route::post('signin', 'AuthenticateController@signIn');
    Route::post('confirmation', 'AuthenticateController@confirmation');
});

Route::group(['middleware' => ['jwt.auth']], function () {

    Route::group([
        'prefix' => 'statistics',
    ], function () {
        Route::get('/', 'StatisticController@filter');
        Route::get('{id}', 'StatisticController@show');
        Route::post('soft-delete/{id}', 'StatisticController@softDelete');
    });

    Route::resource('schema', 'SchemaController');

    Route::group([
        'middleware' => 'role:admin',
        'prefix' => 'admin',
        'namespace' => 'Admin'
    ], function () {
        Route::resource('courses', 'CourseController');
        Route::post('courses/{course}/restore', 'CourseController@restore');

        Route::resource('lessons', 'LessonController');
        Route::post('lessons/{lesson}/restore', 'LessonController@restore');

        Route::resource('units', 'UnitController');
        Route::post('units/{unit}/restore', 'UnitController@restore');

        Route::resource('definitions', 'DefinitionController');
        Route::post('definitions/{definition}/restore', 'DefinitionController@restore');

        Route::group(['prefix' => 'theory'], function () {
            Route::post('/', 'TheoryController@store');
            Route::post('/attachment', 'UploadController@store');
        });

        Route::group(['prefix' => 'test'], function () {
            Route::post('/', 'TestController@store');
        });

    });

    Route::group([
        'middleware' => 'role:user',
        'prefix' => 'user',
        'namespace' => 'User'
    ], function () {
        Route::get('current', 'ExaminationController@current');
        Route::resource('courses', 'CourseController');
        Route::resource('lessons', 'LessonController');
        Route::resource('units', 'UnitController');

        Route::get('definitions', 'DefinitionController@index');

        Route::group(['prefix' => 'examination'], function () {

            Route::get('expires', 'ExaminationController@removeExpired');

            Route::group(['prefix' => 'data'], function () {
                Route::get('theory/{uuid}', 'ExaminationDataController@theory');
                Route::get('schema/{uuid}', 'ExaminationDataController@schema');
                Route::get('test/{uuid}', 'ExaminationDataController@test');

                Route::post('schema/{uuid}', 'ExaminationDataController@saveSchema');
                Route::post('test/{uuid}', 'ExaminationDataController@saveTest');
            });

            Route::post('/', 'ExaminationController@create');
            Route::post('load', 'ExaminationController@load');
            Route::post('status', 'ExaminationController@updateStatus');
            Route::post('tab', 'ExaminationController@updateTab');
            Route::delete('cancel/{uuid}', 'ExaminationController@cancel');

        });

        Route::post('account', 'AccountController@update');

    });

});