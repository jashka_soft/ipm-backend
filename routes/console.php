<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('ipm:truncate', function () {
    \App\Model\Answer::truncate();
    \App\Model\Question::truncate();
    \App\Model\Test::truncate();

    \App\Model\Schema::truncate();
    \App\Model\Theory::truncate();
    \App\Model\Upload::truncate();

    \App\Model\Unit::truncate();
    \App\Model\Lesson::truncate();
    \App\Model\Course::truncate();

    \App\Model\ExaminationInfo::truncate();
    \App\Model\SchemaResult::truncate();
    \App\Model\TestResult::truncate();

    Cache::flush();
})->describe('Clear all data');