
### How to run

copy `.env.example` to `.env`
and setup environment

### local development
Run `docker-compose -f docker-compose-dev.yml up -d`

### if deploy
build only with `docker-compose.yml`