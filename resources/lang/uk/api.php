<?php

return [

    'user_created' => 'Користувача створено',
    'not_confirmed' => 'Акаунт не підтверджено',
    'signin' => 'Ви успішно війшли в акаунт',
    'unauthorized' => 'Не авторизано. Перевірте свої дані',
    'user_not_found' => 'Користувача не знайдено',
    'user_confirmed' => 'Акаунт підтверджено',
    'invalid_token' => 'Невалідний токен доступу',

    'updated' => 'Оновлено',
    'created' => 'Створено',
    'deleted' => 'Видалено',
    'soft_deleted' => 'Видалено',
    'restored' => 'Відновлено',
    'uploaded' => 'Завантажено',

    'access_denied' => 'Доступ заборонено',
    'account_updated' => 'Профіль оновлено',

];
