<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>IT Project Managment</title>
</head>

<body>

<style type="text/css">
    .apple-footer a {
        text-decoration: none !important;
        color: #999999 !important;
        border: none !important;
    }

    .apple-email a {
        text-decoration: none !important;
        color: #448BFF !important;
        border: none !important;
    }
</style>
<div id="wrapper" style="background-color:#ffffff; margin:0 auto; text-align:center; width:100%" bgcolor="#ffffff"
     align="center" width="100%">
    <table class="main-table" align="center"
           style="-premailer-cellpadding:0; -premailer-cellspacing:0; background-color:#ffffff; border:0; margin:0 auto; max-width:480px; mso-table-lspace:0; mso-table-rspace:0; padding:0; text-align:center; width:480"
           cellpadding="0"
           cellspacing="0" bgcolor="#ffffff" width="480">

        <tbody>


        <tr>
            <td class="headline"
                style="color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, san-serif; font-size:40px; font-weight:100; line-height:36px; margin:0 auto; padding:0; text-align:center"
                align="center">IT Project Managment </td>
        </tr>
        <tr>
            <td class="spacer-lg"
                style="-premailer-height:75; -premailer-width:100%; line-height:30px; margin:0 auto; padding:0"
                height="75" width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td class="headline"
                style="color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, san-serif; font-size:30px; font-weight:100; line-height:36px; margin:0 auto; padding:0; text-align:center"
                align="center">Подтвердите аккаунт</td>
        </tr>
        <tr>
            <td class="spacer-sm"
                style="-premailer-height:20; -premailer-width:100%; line-height:10px; margin:0 auto; padding:0"
                height="20" width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td class="copy"
                style="color:#666666; font-family:&quot;Roboto&quot;, Helvetica, Arial, san-serif; font-size:18px; line-height:30px; text-align:center"
                align="center">Перейдите по <a href="{{config('app.front_user_url')}}confirmation/{{$user->confirmed_token}}">этой</a> ссылке что бы подтвердить свой аккаунт.
            </td>
        </tr>

        <tr>
            <td class="spacer-lg"
                style="-premailer-height:75; -premailer-width:100%; line-height:30px; margin:0 auto; padding:0"
                height="75" width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td class="spacer-lg"
                style="-premailer-height:75; -premailer-width:100%; line-height:30px; margin:0 auto; padding:0"
                height="75" width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table class="footer-table"
                       style="-premailer-width:480; background-color:#ececec; margin:0 auto; text-align:center"
                       width="480" bgcolor="#ececec" align="center">
                    <tbody>
                    <tr>
                        <td class="spacer-sm"
                            style="-premailer-height:20; -premailer-width:100%; line-height:10px; margin:0 auto; padding:0"
                            height="20" width="100%">&nbsp;</td>
                    </tr>

                    <tr>
                        <td>
                            <table class="footer-content" align="center"
                                   style="-premailer-width:420; margin:0 auto; padding:0; text-align:center"
                                   width="420">
                                <tbody>
                                <tr>
                                    <td class="footer-text"
                                        style="-webkit-text-size-adjust:100%; color:#999999; font-family:&quot;Roboto&quot;, Helvetica, Arial, san-serif; font-size:10px; line-height:16px; text-align:center"
                                        align="center">

                                        JashkaSoft Inc.
                                        <br><br> This email was sent to <span class="apple-email"><a
                                                    style="border:none; color:#448bff; text-decoration:none"
                                                    target="_blank">{{$user->email}}</a></span> because you register on ITPM
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="spacer-sm"
                            style="-premailer-height:20; -premailer-width:100%; line-height:10px; margin:0 auto; padding:0"
                            height="20" width="100%">&nbsp;</td>
                    </tr>
                    </tbody>
                </table>

            </td>
        </tr>
        </tbody>
    </table>
</div>


</body>