<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('lessons', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('units', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('theories', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('tests', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('uploads', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('questions', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('answers', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('schemas', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('examination_infos', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('schema_results', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('test_results', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('definitions', function (Blueprint $t) {
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $t) {
            $t->dropSoftDeletes();
        });

        Schema::table('lessons', function (Blueprint $t) {
            $t->dropSoftDeletes();
        });

        Schema::table('units', function (Blueprint $t) {
            $t->dropSoftDeletes();
        });

        Schema::table('theories', function (Blueprint $t) {
            $t->dropSoftDeletes();
        });

        Schema::table('tests', function (Blueprint $t) {
            $t->dropSoftDeletes();
        });

        Schema::table('uploads', function (Blueprint $t) {
            $t->dropSoftDeletes();
        });

        Schema::table('questions', function (Blueprint $t) {
            $t->dropSoftDeletes();
        });

        Schema::table('answers', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('schemas', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('examination_infos', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('schema_results', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('test_results', function (Blueprint $t) {
            $t->softDeletes();
        });

        Schema::table('definitions', function (Blueprint $t) {
            $t->softDeletes();
        });
    }
}
