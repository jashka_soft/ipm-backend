<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSchemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schemas', function (Blueprint $t) {
            $t->integer('user_id');
            $t->integer('unit_id')->nullable()->change();
            $t->string('name')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schemas', function (Blueprint $t) {
            $t->dropColumn('user_id');
            $t->integer('unit_id')->nullable(false)->change();
            $t->dropColumn('name');
        });
    }
}
