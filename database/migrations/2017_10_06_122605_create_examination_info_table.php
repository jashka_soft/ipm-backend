<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExaminationInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examination_infos', function (Blueprint $t) {
            $t->increments('id');

            $t->integer('user_id');
            $t->integer('unit_id');
            $t->integer('status');
            $t->string('uuid');
            $t->integer('current_tab');

            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('examination_infos');
    }
}
