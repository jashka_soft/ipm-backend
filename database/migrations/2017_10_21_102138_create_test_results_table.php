<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_results', function (Blueprint $t) {
            $t->increments('id');

            $t->integer('question_id')->unsigned();
            $t->integer('examination_info_id')->unsigned();
            $t->integer('answer_id')->unsigned();
            $t->integer('correct_answer_id')->unsigned();

            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('test_results');
    }
}
