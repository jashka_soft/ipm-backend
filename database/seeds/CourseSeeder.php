<?php

use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Answer::truncate();
        \App\Model\Question::truncate();
        \App\Model\Test::truncate();

        \App\Model\Schema::truncate();
        \App\Model\Theory::truncate();


        \App\Model\Unit::truncate();
        \App\Model\Lesson::truncate();
        \App\Model\Course::truncate();


        $user = \App\Model\User::whereRole('admin')->first();
        $course = $this->createCourse($user);
        $lesson = $this->createLesson($course);
        $unit = $this->createUnit($lesson);

        $this->createTheory($unit);
        $this->createSchema($unit, $user->id);
        $this->createTest($unit);
    }

    /**
     * @return string
     */
    public function generateUuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @param \App\Model\Unit $unit
     */
    public function createTest(\App\Model\Unit $unit)
    {
        $test = \App\Model\Test::create([
            'name' => 'Имя теста',
            'unit_id' => $unit->id
        ]);

        for ($i = 1; $i <= 10; ++$i)
        {
            $this->createQuestion($test, $i);
        }
    }

    /**
     * @param \App\Model\Test $test
     * @param $index
     */
    public function createQuestion(\App\Model\Test $test, $index)
    {
        $question = \App\Model\Question::create([
            'name' => 'Вопрос ' . $index,
            'answer_id' => 1,
            'test_id' => $test->id
        ]);

        $answers = [];

        for ($i = 1; $i <= 4; ++$i)
        {
            $answers[] = $this->createAnswer($question, $i);
        }

        $question->answer_id = $answers[0]->id;
        $question->save();
    }

    /**
     * @param \App\Model\Question $question
     * @param $index
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createAnswer(\App\Model\Question $question, $index)
    {
        return \App\Model\Answer::create([
            'name' => 'Ответ ' . $index,
            'question_id' => $question->id
        ]);
    }

    /**
     * @param \App\Model\Unit $unit
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createSchema(\App\Model\Unit $unit, int $userId)
    {
        $diagram = <<<EOT
{
    "class": "go.TreeModel",
    "nodeDataArray": [
      {
        "key": -1,
        "name": "asdasdas 1",
        "title": null,
        "hash": "df78a37c-5b76-4205-55b3-3ff357fc3a3d",
        "choice": true
      },
      {
        "key": -2,
        "name": "(название)",
        "title": null,
        "hash": "6f206b8f-800d-19e5-e9d6-3c1438064c59",
        "parent": -1
      },
      {
        "key": -3,
        "name": "(название)",
        "title": null,
        "hash": "e882189e-9b82-2f4d-7068-8da8ca7d14e1",
        "parent": -2
      },
      {
        "key": -4,
        "name": "(название)",
        "title": null,
        "hash": "311575e7-25b3-db73-d68f-7c732579f787",
        "parent": -2,
        "choice": false
      },
      {
        "key": -5,
        "name": "(название)",
        "title": null,
        "hash": "482e82a5-09d5-ea4c-9ac4-47e77443b564",
        "parent": -2,
        "choice": false
      },
      {
        "key": -6,
        "name": "(название)",
        "title": null,
        "hash": "12ce25c9-5203-a10b-00e0-8b1cefaca4f9",
        "parent": -2
      },
      {
        "key": -7,
        "name": "(название)",
        "title": null,
        "hash": "1596a3fd-28cf-793a-9a46-204ebe4d2eaa",
        "parent": -3
      },
      {
        "key": -8,
        "name": "123123",
        "title": null,
        "hash": "ef3e115e-b739-90bb-7701-f1e0ebbe3bcc",
        "parent": -3,
        "choice": true
      },
      {
        "key": -9,
        "name": "(название)",
        "title": null,
        "hash": "c7c0338c-a9a2-dca7-237d-559fa2bcfd55",
        "parent": -5
      }
    ]
  }
EOT;

        return \App\Model\Schema::create([
            'type' => 'WBSOBS',
            'diagram' => json_decode($diagram),
            'unit_id' => $unit->id,
            'user_id' => $userId
        ]);
    }

    /**
     * @param \App\Model\Unit $unit
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createTheory(\App\Model\Unit $unit)
    {
        return \App\Model\Theory::create([
            'iframe' => '<iframe src="//www.slideshare.net/slideshow/embed_code/key/BAAb3YtOP9VSWA" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/rdohms/your-code-sucks-lets-fix-it-cakefest2012" title="Your code sucks, let&#x27;s fix it (CakeFest2012)" target="_blank">Your code sucks, let&#x27;s fix it (CakeFest2012)</a> </strong> from <strong><a href="https://www.slideshare.net/rdohms" target="_blank">Rafael Dohms</a></strong> </div>',
            'unit_id' => $unit->id
        ]);
    }

    /**
     * @param \App\Model\Lesson|\App\Model\Unit $unit
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createUnit(\App\Model\Lesson $unit)
    {
        return \App\Model\Unit::create([
            'name' => 'Юнит (тест)',
            'description' => 'Описание юнита',
            'lesson_id' => $unit->id
        ]);
    }

    /**
     * @param \App\Model\Course $course
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createLesson(\App\Model\Course $course)
    {
        return \App\Model\Lesson::create([
            'name' => 'Урок (тест)',
            'description' => 'Описание урока',
            'course_id' => $course->id
        ]);
    }

    /**
     * @param \App\Model\User $user
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createCourse(\App\Model\User $user)
    {
        return \App\Model\Course::create([
            'name' => 'Курс (тест)',
            'description' => 'Описание курса',
            'user_id' => $user->id
        ]);
    }
}
