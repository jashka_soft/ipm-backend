<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\User::truncate();

        \App\Model\User::firstOrCreate(
            [
                'email' => 'admin@g.com'
            ],
            [
                'name' => 'Admin',
                'email' => 'admin@g.com',
                'role' => 'admin',
                'confirmed' => true,
                'password' => 'admin',
            ]
        );

        \App\Model\User::firstOrCreate(
            [
                'email' => 'user@g.com'
            ],
            [
                'name' => 'User',
                'email' => 'user@g.com',
                'role' => 'user',
                'confirmed' => true,
                'password' => 'user',
            ]
        );

        \App\Model\User::firstOrCreate(
            [
                'email' => 'user1@g.com'
            ],
            [
                'name' => 'User1',
                'email' => 'user1@g.com',
                'role' => 'user',
                'confirmed' => true,
                'password' => 'user',
            ]
        );

    }
}
