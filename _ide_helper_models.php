<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Model {
    /**
     * App\Model\User
     *
     * @property int $id
     * @property string $name
     * @property string $email
     * @property string $password
     * @property string $role
     * @property string $remember_token
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereCreatedAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereEmail($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereName($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User wherePassword($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereRememberToken($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereRole($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereUpdatedAt($value)
     * @mixin \Eloquent
     * @property bool $confirmed
     * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereConfirmed($value)
     */
    class User extends \Eloquent
    {
    }
}

