<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Scopes\OrderByDeletedAtScope;

/**
 * App\Model\Cource
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Image[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Lesson[] $lessons
 * @property-read \App\Model\Image $image
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course deleteAtOrder(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course withTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Course orderByDeleted(\Illuminate\Database\Eloquent\Model $model)
 */
class Course extends Model
{
    use SoftDeletes;
    use OrderByDeletedAtScope;

    protected $fillable = ['name', 'user_id', 'description'];

    protected $dates = ['deleted_at'];

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}
