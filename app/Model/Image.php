<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Behavior\Image
 *
 * @property int $id
 * @property string $image
 * @property int $imageable_id
 * @property string $imageable_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Image whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Image whereImageableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Image whereImageableType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Image whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $imageable
 */
class Image extends Model
{
    public function imageable()
    {
        return $this->morphTo();
    }
}
