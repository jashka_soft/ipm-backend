<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Scopes\OrderByDeletedAtScope;

/**
 * App\Model\ExaminationInfo
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $unit_id
 * @property int $status
 * @property string $uuid
 * @property int $current_tab
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Answer[] $testAnswers
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereCurrentTab($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereUnitId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereUuid($value)
 * @property-read \App\Model\SchemaResult $schema
 * @property-read \App\Model\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\TestResult[] $test
 * @property-read \App\Model\Unit $unit
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo started()
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo tab()
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo end()
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo deleteAtOrder(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo withTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\ExaminationInfo orderByDeleted(\Illuminate\Database\Eloquent\Model $model)
 */
class ExaminationInfo extends Model
{
    use SoftDeletes;
    use OrderByDeletedAtScope;

    const STARTED = 1;
    const END = 2;

    const EMPTY_TAB = 1;
    const THEORY = 2;
    const SCHEMA = 3;
    const TEST = 4;

    const MAX_COUNT_EXAMINATIONS = 3;
    const MAX_MINUTES = 20;

    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'unit_id', 'uuid', 'status', 'current_tab'];

    public function getIsSoftDeletedAttribute()
    {
        return (boolean)$this->deleted_at;
    }

    public function scopeStarted()
    {
        return $this->where('status', '=', ExaminationInfo::STARTED);
    }

    /**
     * @return $this
     */
    public function scopeEnd()
    {
        return $this->where('status', '=', ExaminationInfo::END);
    }

    /**
     * @param Builder $builder
     * @param int $tab
     * @return Builder
     */
    public function scopeTab(Builder $builder, int $tab)
    {
        return $builder->where('current_tab', '=', $tab);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function schema()
    {
        return $this->hasOne(SchemaResult::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function test()
    {
        return $this->hasMany(TestResult::class);
    }
}
