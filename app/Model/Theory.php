<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\Theory
 *
 * @property int $id
 * @property string $iframe
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory whereIframe($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Model\Unit $unit
 * @property int $unit_id
 * @property-read \App\Model\Upload $file
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory whereUnitId($value)
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Theory withTrashed(\Illuminate\Database\Eloquent\Model $model)
 */
class Theory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['iframe', 'unit_id'];

    protected $with = ['file'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Theory $test) {
            $test->unit()->delete();
            $test->file()->delete();
        });
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function file()
    {
        return $this->hasOne(Upload::class);
    }
}
