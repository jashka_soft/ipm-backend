<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\Schema
 *
 * @property int $id
 * @property int $unit_id
 * @property mixed $diagram
 * @property mixed $palette
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema whereDiagram($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema wherePalette($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema whereUnitId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $type
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema whereType($value)
 * @property-read \App\Model\Unit $unit
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema withTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Schema onlyUser($userId)
 */
class Schema extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['unit_id', 'diagram', 'type', 'user_id', 'name'];

    protected $casts = [
        'diagram' => 'json',
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function setDiagramAttribute($value)
    {
        $this->attributes['diagram'] = json_encode($value);
    }

    public function scopeOnlyUser(Builder $builder, int $userId)
    {
        return $builder->where('user_id', $userId);
    }

    public static function cipher(string $key, string $str): string
    {
        $s = [];
        for ($i = 0; $i < 256; $i++) {
            $s[$i] = $i;
        }
        $j = 0;
        for ($i = 0; $i < 256; $i++) {
            $j = ($j + $s[$i] + ord($key[$i % strlen($key)])) % 256;
            $x = $s[$i];
            $s[$i] = $s[$j];
            $s[$j] = $x;
        }
        $i = 0;
        $j = 0;
        $res = '';
        for ($y = 0; $y < strlen($str); $y++) {
            $i = ($i + 1) % 256;
            $j = ($j + $s[$i]) % 256;
            $x = $s[$i];
            $s[$i] = $s[$j];
            $s[$j] = $x;
            $res .= $str[$y] ^ chr($s[($s[$i] + $s[$j]) % 256]);
        }
        return $res;
    }

    /*public function type()
    {
        return $this->hasOne(SchemaType::class);
    }*/

}
