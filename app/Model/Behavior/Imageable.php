<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 24.06.17
 * Time: 19:24
 */

namespace App\Model\Behavior;


use App\Model\Image;

/**
 * Class Imageable
 * @package App\Model\Behavior
 */
trait Imageable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}