<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 16.02.18
 * Time: 21:38
 */

namespace App\Model\Behavior;


trait GetIds
{
    public function getIds(array $ids, string $columnWhere, string $columnSelect = 'id'): array
    {
        return $this->model
            ->withTrashed()
            ->whereIn($columnWhere, $ids)
            ->pluck($columnSelect)
            ->toArray();
    }
}