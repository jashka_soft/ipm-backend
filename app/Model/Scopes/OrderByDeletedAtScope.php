<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 14.01.18
 * Time: 16:20
 */

namespace App\Model\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait OrderByDeletedAtScope
{
    public function scopeOrderByDeleted (Builder $builder)
    {
        return $builder->orderBy('deleted_at', 'ASC');
    }
}