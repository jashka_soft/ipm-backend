<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Scopes\OrderByDeletedAtScope;

/**
 * App\Model\Lesson
 *
 * @property int $id
 * @property string $name
 * @property int $cource_id
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereCourceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Model\Course $cource
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Unit[] $units
 * @property int $course_id
 * @property-read \App\Model\Course $course
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereCourseId($value)
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson deleteAtOrder(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson withTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Lesson orderByDeleted(\Illuminate\Database\Eloquent\Model $model)
 */
class Lesson extends Model
{
    use SoftDeletes;
    use OrderByDeletedAtScope;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'description', 'course_id'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Lesson $test) {
            $test->units()->delete();
        });
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function units()
    {
        return $this->hasMany(Unit::class);
    }
}
