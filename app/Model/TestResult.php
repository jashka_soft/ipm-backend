<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TestResult
 *
 * @package App\Model
 * @property int $id
 * @property int $question_id
 * @property int $examination_info_id
 * @property int $answer_id
 * @property int $correct_answer_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereAnswerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereCorrectAnswerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereExaminationInfoId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Model\Answer $answer
 * @property-read \App\Model\Answer $correct_answer
 * @property-read \App\Model\ExaminationInfo $examination
 * @property-read \App\Model\Question $question
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TestResult withTrashed(\Illuminate\Database\Eloquent\Model $model)
 */
class TestResult extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['question_id', 'examination_info_id', 'answer_id', 'correct_answer_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examination()
    {
        return $this->belongsTo(ExaminationInfo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function correct_answer()
    {
        return $this->belongsTo(Answer::class, 'correct_answer_id');
    }
}
