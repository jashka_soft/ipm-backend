<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Scopes\OrderByDeletedAtScope;

/**
 * App\Model\Unit
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $lesson_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit whereLessonId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Model\Lesson $lesson
 * @property-read \App\Model\Theory $theory
 * @property-read \App\Model\Test $test
 * @property-read \App\Model\Schema $schema
 * @property-read mixed $examinations
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit deleteAtOrder(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit withTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Unit orderByDeleted(\Illuminate\Database\Eloquent\Model $model)
 */
class Unit extends Model
{
    use SoftDeletes;
    use OrderByDeletedAtScope;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'description', 'lesson_id'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Unit $test) {
            $test->theory()->delete();
            $test->schema()->delete();
            $test->test()->delete();
        });
    }


    public function getUsersActiveAttribute()
    {
        $userIds = ExaminationInfo::started()
            ->select(['unit_id', 'user_id'])
            ->where('unit_id', $this->id)
            ->pluck('user_id')
            ->toArray();

        return User::whereIn('id', $userIds)->get();
    }

    public function getExaminationsAttribute()
    {
        return ExaminationInfo::whereUnitId($this->id)
            ->whereUserId(auth()->user()->id)
            ->get();
    }

    public function getCurrentExaminationAttribute()
    {
        return ExaminationInfo::where('status', ExaminationInfo::STARTED)
            ->whereUnitId($this->id)
            ->whereUserId(auth()->user()->id)
            ->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function theory()
    {
        return $this->hasOne(Theory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->hasOne(Test::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function schema()
    {
        return $this->hasOne(Schema::class);
    }
}
