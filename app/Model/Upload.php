<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\Upload
 *
 * @property int $id
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $theory_id
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload whereTheoryId($value)
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Upload withTrashed(\Illuminate\Database\Eloquent\Model $model)
 */
class Upload extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['path', 'theory_id'];

    public static function file($content, $extension)
    {
        $fileName = str_random(8) . '.' . $extension;

        \Storage::disk('s3')->put('attachments/theories/' . $fileName, $content);

        return 'attachments/theories/' . $fileName;

    }
}
