<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Test
 *
 * @package App\Model
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Question[] $questions
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $unit_id
 * @property string $hash
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test whereHash($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test whereUnitId($value)
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Test withTrashed(\Illuminate\Database\Eloquent\Model $model)
 */
class Test extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = ['unit_id'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Test $test) {
            $test->questions()->delete();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * @param array $questions
     */
    public function fillQuestions(array $questions)
    {
        foreach ($questions as $question) {
            $answers = $question['answers'];
            if (array_key_exists('id', $question)) {
                $qn = Question::where('id', '=', $question['id'])->first();
                $qn->update((array)$question);

                $qn->fillAnswers($answers);

            } else {
                $newQuestion = Question::create(array_merge($question, ['answer_id' => 1, 'test_id' => $this->id]));

                $answers = $newQuestion->fillAnswers($answers);

                $newQuestion->answer_id = $answers[$question['answer_id']]->id;
                $newQuestion->save();
            }
        }
    }

}
