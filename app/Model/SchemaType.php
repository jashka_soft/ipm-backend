<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 19.10.17
 * Time: 20:28
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

/**
 * Class SchemaType
 *
 * @package App\Model
 * @property int $id
 * @property string $type
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaType whereType($value)
 * @mixin \Eloquent
 */
class SchemaType extends Model
{
    public $timestamps = false;

    protected $fillable = ['type'];

}