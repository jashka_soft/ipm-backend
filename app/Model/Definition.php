<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Scopes\OrderByDeletedAtScope;

/**
 * App\Model\Definition
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition deleteAtOrder(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition withTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Definition orderByDeleted(\Illuminate\Database\Eloquent\Model $model)
 */
class Definition extends Model
{
    use SoftDeletes;
    use OrderByDeletedAtScope;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'description'];
}
