<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Question
 *
 * @package App\Model
 * @property int $id
 * @property string $name
 * @property string $hash
 * @property int $test_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Answer[] $answers
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereHash($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereTestId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $answer_id
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereAnswerId($value)
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Question withTrashed(\Illuminate\Database\Eloquent\Model $model)
 */
class Question extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'answer_id', 'test_id'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Question $test) {
            $test->answers()->delete();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    /**
     * @param array $questionAnswers
     * @return Answer[]
     */
    public function fillAnswers(array $questionAnswers)
    {
        $answers = [];

        foreach ($questionAnswers as $answer) {
            if (array_key_exists('id', $answer)) {
                $findAnswer = Answer::where('id', '=', $answer['id'])->first();
                $findAnswer->fill($answer);
                $findAnswer->save();
                $answers[] = $findAnswer;
            } else {
                $answers[] = Answer::create(array_merge($answer, ['question_id' => $this->id]));
            }
        }

        return $answers;
    }
}
