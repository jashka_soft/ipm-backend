<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 10.10.17
 * Time: 14:22
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SchemaResults
 *
 * @package App\Model
 * @property int $id
 * @property int $examination_info_id
 * @property int $schema_id
 * @property string $diagram
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult whereDiagram($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult whereExaminationInfoId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult whereSchemaId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Model\ExaminationInfo $examination
 * @property \Carbon\Carbon $deleted_at
 * @property-read mixed $type
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\SchemaResult withTrashed(\Illuminate\Database\Eloquent\Model $model)
 */
class SchemaResult extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['examination_info_id', 'schema_id', 'diagram'];

    protected $casts = [
        'diagram' => 'json',
    ];

    protected $appends = ['type'];

    public function getTypeAttribute()
    {
        return Schema::withTrashed()->find($this->schema_id)->type;
    }

    public function examination()
    {
        return $this->belongsTo(ExaminationInfo::class);
    }
}