<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Answer
 *
 * @package App\Model
 * @property int $id
 * @property string $name
 * @property string $hash
 * @property int $question_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer whereHash($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer whereQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer onlyTrashed(\Illuminate\Database\Eloquent\Model $model)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Answer withTrashed(\Illuminate\Database\Eloquent\Model $model)
 */
class Answer extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'question_id'];

    protected $dates = ['deleted_at'];

}
