<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 18.06.17
 * Time: 8:30
 */

namespace App\Mail;


use App\Model\User;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;


/**
 * Class Confirmation
 * @package App\Mail
 */
class Confirmation
{
    /**
     * @var \Mail
     */
    private $mailer;

    /**
     * Confirmation constructor.
     * @param Mailer|\Mail $mail
     */
    public function __construct(Mailer $mail)
    {
        $this->mailer = $mail;
    }

    /**
     * @param User $user
     */
    public function send(User $user)
    {
        $this->mailer->send('emails.confirmation', compact('user'), function (Message $message) use ($user) {
            $message->from(config('mail.from.address'), config('app.name'));
            $message->to($user->email, $user->name)->subject('Подтвердите свой аккаунт');
        });
    }
}