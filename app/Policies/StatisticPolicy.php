<?php

namespace App\Policies;

use App\Model\ExaminationInfo;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StatisticPolicy
{
    use HandlesAuthorization;

    const CAN_SOFT_DELETE = 'softDelete';
    const CAN_VIEW = 'view';

    public function softDelete(User $user, ExaminationInfo $statistic)
    {
        return $user->role === User::ROLE_ADMIN;
    }

    public function view(User $user, ExaminationInfo $statistic)
    {
        return ($user->id === $statistic->user_id && !$statistic->is_soft_deleted)
            || $this->softDelete($user, $statistic);
    }
}
