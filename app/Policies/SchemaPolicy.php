<?php

namespace App\Policies;

use App\Model\Schema;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchemaPolicy
{
    use HandlesAuthorization;

    const CAN_DELETE = 'delete';
    const CAN_VIEW = 'view';

    /**
     * @param User $user
     * @param Schema $schema
     * @return bool
     */
    public function delete(User $user, Schema $schema)
    {
        return $this->checkPermission($user->id, $schema);
    }

    /**
     * @param User $user
     * @param Schema $schema
     * @return bool
     */
    public function view(User $user, Schema $schema)
    {
        return $this->checkPermission($user->id, $schema);
    }

    /**
     * @param $userId
     * @param Schema $schema
     * @return bool
     */
    private function checkPermission($userId, Schema $schema)
    {
        return $schema->user_id === $userId && $schema->unit_id === null;
    }
}
