<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 19.10.17
 * Time: 19:22
 */

namespace Services;


use App\Model\TestResult;

/**
 * Class TestService
 * @package Services
 */
class TestService
{
    /**
     * @param $test
     * @return mixed
     */
    public function randomize($test)
    {
        foreach ($test['questions'] as $index => $question) {
            shuffle($question['answers']);
            $test['questions'][$index]['answers'] = $question['answers'];
        }

        shuffle($test['questions']);

        return $test;

    }

    /**
     * @param $test
     * @return mixed
     */
    public function hideHashFromQuestion($test)
    {
        foreach ($test['questions'] as $index => $question) {
            unset($test['questions'][$index]['answer_id']);
        }

        return $test;
    }

    /**
     * @param array $question
     * @param array $originalQuestions
     * @return int
     */
    public function getRightAnswer(array $question, array $originalQuestions)
    {
        foreach ($originalQuestions as $originalQuestion) {
            if ($originalQuestion['id'] === $question['id']) {
                return $originalQuestion['answer_id'];
            }
        }
    }

    /**
     * @param $questions
     * @param $originalQuestions
     * @param $examinationId
     * @return array
     */
    public function createAnswers(array $questions, array $originalQuestions, int $examinationId): array
    {
        $results = [];

        foreach ($questions as $question) {
            $results[] = [
                'question_id' => $question['id'],
                'examination_info_id' => $examinationId,
                'answer_id' => $question['answer_id'],
                'correct_answer_id' => $this->getRightAnswer($question, $originalQuestions)
            ];
        }

        TestResult::insert($results);

        return $results;
    }
}