<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\Lesson;

class LessonController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function show($id, Lesson $lesson)
    {
        $data = $lesson->whereCourseId($id)->paginate();

        return response()->json(compact('data'));
    }
}
