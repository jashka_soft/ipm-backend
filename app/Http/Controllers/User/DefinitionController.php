<?php

namespace App\Http\Controllers\User;

use App\Model\Definition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repository\DefinitionRepository;

class DefinitionController extends Controller
{
    /**
     * @var DefinitionRepository
     */
    private $definitionRepository;

    /**
     * DefinitionController constructor.
     * @param DefinitionRepository $definitionRepository
     */
    public function __construct(DefinitionRepository $definitionRepository)
    {
        $this->definitionRepository = $definitionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Definition $definition
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Definition $definition)
    {
        $data = $definition->paginate();
        $data->setPath($request->fullUrl());

        return response()->json(compact('data'));
    }
}
