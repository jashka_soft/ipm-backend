<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\AccountUpdateRequest;
use App\Model\User;
use Tymon\JWTAuth\JWTAuth;

class AccountController extends Controller
{
    /**
     * @var null|JWTAuth
     */
    protected $auth = null;

    /**
     * AccountController constructor.
     * @param JWTAuth $JWTAuth
     */
    public function __construct(JWTAuth $JWTAuth)
    {
        $this->auth = $JWTAuth;
    }

    /**
     * @param AccountUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AccountUpdateRequest $request)
    {

        $user = auth()->user();

        $user->password = $request->get('password');

        $user->save();

        $token = $this->auth->attempt([
            'email' => $user->email,
            'password' => $request->get('password')
        ]);

        return response()->json([
            'data' => [
                'token' => $token
            ],
            'message' => trans('api.account_updated')
        ]);
    }
}
