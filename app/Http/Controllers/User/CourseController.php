<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\Course;

class CourseController extends Controller
{
    public function index(Course $course)
    {
        $data = $course->paginate();

        return response()->json(compact('data'));
    }
}
