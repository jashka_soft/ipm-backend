<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Unit $unit
     * @return \Illuminate\Http\Response
     */
    public function show($id, Unit $unit)
    {
        $data = $unit->whereLessonId($id)->paginate();

        foreach ($data as $unit) {
            $unit->append([
                'examinations',
                'current_examination'
            ]);
        }

        return response()->json(compact('data'));
    }
}
