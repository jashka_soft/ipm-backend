<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\Question;
use App\Model\Schema;
use App\Model\SchemaResult;
use App\Model\Test;
use App\Model\Theory;
use Illuminate\Http\Request;
use Repository\ExaminationInfoRepository;
use Repository\SchemaRepository;
use Services\TestService;

class ExaminationDataController extends Controller
{
    /**
     * @var TestService
     */
    private $testService;
    /**
     * @var SchemaRepository
     */
    private $schemaRepository;
    /**
     * @var ExaminationInfoRepository
     */
    private $examinationRepository;

    /**
     * ExaminationDataController constructor.
     * @param TestService $testService
     * @param SchemaRepository $schemaRepository
     * @param ExaminationInfoRepository $examinationRepository
     */
    public function __construct(
        TestService $testService,
        SchemaRepository $schemaRepository,
        ExaminationInfoRepository $examinationRepository)
    {
        $this->testService = $testService;
        $this->schemaRepository = $schemaRepository;
        $this->examinationRepository = $examinationRepository;
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function theory(string $uuid)
    {
        $unitId = (int)\Cache::get('examination_' . $uuid);

        return response()->json([
            'data' => Theory::whereUnitId($unitId)->with('file')->first()
        ]);
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function schema(string $uuid)
    {
        $unitId = (int)\Cache::get('examination_' . $uuid);

        $examination = $this->examinationRepository->getActiveExamination($uuid);

        if ($examination->schema()->exists()) {
            $examination->with(['schema']);
            $schema = $examination->schema;

            $type = Schema::withTrashed()->whereId($schema->schema_id)->first(['type'])->type;
            $schema->type = $type;

            return response()->json([
                'data' => $this->cryptSchemaDiagram($schema)
            ]);
        }

        $schema = Schema::withTrashed()->whereUnitId($unitId)->first();

        return response()->json([
            'data' => $this->cryptSchemaDiagram($schema)
        ]);
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function test(string $uuid)
    {
        $unitId = (int)\Cache::get('examination_' . $uuid);

        $test = Test::withTrashed()->whereUnitId($unitId)->with('questions.answers')->first()->toArray();
        $test = $this->testService->randomize($test);
        $test = $this->testService->hideHashFromQuestion($test);

        return response()->json([
            'data' => $test
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveSchema(Request $request)
    {
        $examination = $this->examinationRepository->getActiveExamination($request->get('uuid'));

        $schema = null;

        if (!$examination->schema()->exists()) {

            $schema = SchemaResult::create([
                'examination_info_id' => $examination->id,
                'schema_id' => $request->get('schema_id'),
                'diagram' => $request->get('schema')
            ]);

            return response()->json([
                'data' => $schema,
                'message' => 'examination.schema_saved'
            ]);
        }

        $schema = SchemaResult::withTrashed()->whereId($request->get('schema_id'))->first();

        return response()->json([
            'data' => $schema
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTest(Request $request)
    {
        $examination = $this->examinationRepository->getActiveExamination($request->get('uuid'));

        if (!$examination->test()->exists()) {
            $questions = Question::withTrashed()->whereTestId($request->get('test_id'))->get();

            $this->testService->createAnswers($request->get('questions'), $questions->toArray(), $examination->id);
        }

        return response()->json([
            'data' => $request->all(),
            'message' => 'examination.test_saved'
        ]);
    }

    /**
     * @param Schema|SchemaResult $schema
     * @return Schema|array
     */
    private function cryptSchemaDiagram($schema) {
        $schema = $schema->toArray();
        $schema['diagram'] = utf8_encode(Schema::cipher(auth()->user()->id, json_encode($schema['diagram'])));
        return $schema;
    }
}
