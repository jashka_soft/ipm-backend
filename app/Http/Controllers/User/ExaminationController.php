<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExaminationRequest;
use App\Model\ExaminationInfo;
use App\Model\Unit;
use Illuminate\Http\Request;
use Repository\ExaminationInfoRepository;

class ExaminationController extends Controller
{
    /**
     * @var ExaminationInfoRepository
     */
    private $examinationRepository;

    /**
     * ExaminationController constructor.
     * @param ExaminationInfoRepository $examinationRepository
     */
    public function __construct(
        ExaminationInfoRepository $examinationRepository
    )
    {
        $this->examinationRepository = $examinationRepository;
    }

    public function current()
    {
        $examinations = $this->examinationRepository->getActiveExaminations();

        return response()->json([
            'data' => $examinations
        ]);
    }

    public function removeExpired()
    {
        $uuids = $this->examinationRepository->removeExpired();

        return response()->json([
            'data' => $uuids,
        ]);
    }

    public function create(Request $request)
    {
        $unitId = (int)$request->get('unit_id');
        $userId = auth()->user()->id;

        $count = $this->examinationRepository->getCountExaminations(
            $userId,
            $unitId
        );

        if ($count === ExaminationInfo::MAX_COUNT_EXAMINATIONS) {
            return response()->json([
                'message' => trans('examination.max_count', [
                    'max' => ExaminationInfo::MAX_COUNT_EXAMINATIONS
                ])
            ], 400);
        }

        $examination = $this
            ->examinationRepository
            ->create($unitId, $userId);

        \Cache::forever('examination_' . $examination->uuid, $unitId);

        $unit = Unit::find($unitId);
        $unit->append('examinations');

        return response()->json([
            'data' => compact('examination', 'unit'),
            'message' => trans('examination.load')
        ]);
    }

    public function load(ExaminationRequest $request)
    {
        $examination = $this->examinationRepository->examination($request->get('uuid'));

        if ($examination === null) {
            return response()->json([
               'message' => trans('examination.not_found')
            ], 404);
        }

        if ($examination->status === ExaminationInfo::END) {
            return response()->json([
                'message' => trans('examination.end')
            ], 400);
        }

        $unit = Unit::find($examination->unit_id);
        $unit->append('examinations');

        return response()->json([
            'data' => compact('examination', 'unit'),
            'message' => trans('examination.load')
        ]);
    }

    public function updateTab(Request $request)
    {
        $examination = $this->examinationRepository->getActiveExamination($request->get('uuid'));

        $tab = (int)$request->get('current_tab');

        if ($examination->current_tab === ExaminationInfo::TEST && $tab < $examination->current_tab) {
            return response()->json([
                'message' => 'Понижать нельзя (Про статус конечно же)'
            ], 403);
        }

        return response()->json([
            'data' => $this->examinationRepository->updateTab($examination, $tab)
        ]);
    }

    public function updateStatus(Request $request)
    {
        $ex = $this->examinationRepository->getActiveExamination($request->get('uuid'));
        $status = (int)$request->get('status');

        if ($ex->status < $status) {
            return response()->json([
                'data' => $this->examinationRepository->updateStatus($ex, $status)
            ]);
        }

        return response()->json([
            'message' => 'Понижать нельзя (Это не том о чем ты подумал)'
        ], 403);
    }

    public function cancel($uuid)
    {
        $this->examinationRepository->cancel($uuid);
        \Cache::forget('examination_' . $uuid);

        return response()->json([
            'message' => 'examination.cancel'
        ]);
    }
}
