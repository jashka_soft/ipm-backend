<?php

namespace App\Http\Controllers;

use App\Model\ExaminationInfo;
use App\Policies\StatisticPolicy;
use Illuminate\Cache\Repository as CacheRepository;
use Illuminate\Http\Request;
use Repository\ExaminationInfoRepository;
use Repository\StatisticsRepository;

class StatisticController extends Controller
{
    /**
     * @var ExaminationInfoRepository
     */
    private $statisticsRepository;

    /**
     * @var CacheRepository
     */
    private $cacheRepository;

    /**
     * StatisticController constructor.
     * @param StatisticsRepository $statisticsRepository
     * @param CacheRepository $cacheRepository
     * @internal param CacheManager $cacheManager
     * @internal param ExaminationInfo $examinationInfo
     */
    public function __construct(StatisticsRepository $statisticsRepository, CacheRepository $cacheRepository)
    {
        $this->statisticsRepository = $statisticsRepository;
        $this->cacheRepository = $cacheRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        $data = $this->statisticsRepository->search($request->all(), auth()->user());

        return response()->json(compact('data'));
    }

    /**
     * @param $id
     * @param ExaminationInfo $examinationInfo
     * @return \Illuminate\Http\JsonResponse
     * @internal param ExaminationInfo $statistic
     */
    public function show($id, ExaminationInfo $examinationInfo)
    {
        $user = auth()->user();
        $statistic = $examinationInfo->withTrashed()->find($id);

        if (!$user->can(StatisticPolicy::CAN_VIEW, $statistic)) {
            return response()->json([
                'message' => trans('api.access_denied')
            ], 403);
        }

        $key = 'result_' . $id;

        if (!$this->cacheRepository->has($key)) {
            $this->cacheRepository->forever($key, $this->statisticsRepository->loadResult($id, auth()->user()));
        }

        $data = $this->cacheRepository->get($key);

        return response()->json(compact('data'));
    }

    public function softDelete(int $id, ExaminationInfo $examinationInfo)
    {
        $user = auth()->user();
        $statistic = $examinationInfo->withTrashed()->find($id);

        if (!$user->can(StatisticPolicy::CAN_SOFT_DELETE, $statistic)) {
            return response()->json([
                'message' => trans('api.access_denied')
            ], 403);
        }

        $this->statisticsRepository->softDelete($id);

        return response()->json([
            'message' => trans('api.soft_deleted')
        ]);
    }
}
