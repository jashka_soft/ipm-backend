<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfirmationRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\Register;
use App\Mail\Confirmation;
use App\Model\User;
use Tymon\JWTAuth\JWTAuth;

/**
 * Class AuthenticateController
 * @package App\Http\Controllers
 */
class AuthenticateController extends Controller
{
    /**
     * @var null|JWTAuth
     */
    protected $auth = null;

    /**
     * AuthenticateController constructor.
     * @param JWTAuth $JWTAuth
     */
    public function __construct(JWTAuth $JWTAuth)
    {
        $this->auth = $JWTAuth;
    }

    /**
     * @param Register $register
     * @param User $user
     * @param Confirmation $confirmation
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUp(Register $register, User $user, Confirmation $confirmation)
    {
        $user->fill($register->all());
        $user->role = User::ROLE_USER;
        $user->confirmed = false;
        $user->confirmed_token = hash('md5', $user->email . time());
        $user->save();

        $confirmation->send($user);

        return response()->json([
            'data' => compact('user'),
            'message' => trans('api.user_created')
        ]);
    }

    /**
     * @param LoginRequest $request
     * @param User $query
     * @return \Illuminate\Http\JsonResponse
     */
    public function signIn(LoginRequest $request, User $query)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        $credentials = compact('email', 'password');

        $user = $query->select(['email', 'confirmed'])->whereEmail($email)->first();

        if ($user && !$user->confirmed) {
            return response()->json([
                'message' => trans('api.not_confirmed')
            ], 401);
        }

        if ($token = $this->auth->attempt($credentials)) {
            $user = $this->auth->toUser($token);

            return response()->json([
                'data' => compact('user', 'token'),
                'message' => trans('api.signin')
            ]);
        }

        return response()->json([
            'message' => trans('api.unauthorized')
        ], 401);
    }

    /**
     * @param ConfirmationRequest $confirmationRequest
     * @param User $query
     * @return \Illuminate\Http\JsonResponse
     * @internal param $email
     */
    public function confirmation(ConfirmationRequest $confirmationRequest, User $query)
    {

        $user = $query
            ->where('confirmed_token', $confirmationRequest->get('token'))
            ->first();

        if (!$user) {
            return response()->json([
                'message' => trans('api.user_not_found')
            ], 404);
        }

        if ($user->confirmed) {
            return response()->json([
                'message' => trans('api.user_confirmed')
            ], 401);
        }

        if ($user->confirmed_token !== $confirmationRequest->get('token')) {
            return response()->json([
                'message' => trans('api.invalid_token')
            ], 401);
        }

        $user->confirmed_token = null;
        $user->confirmed = true;
        $user->save();

        return response()->json([
            'message' => trans('api.user_confirmed')
        ]);

    }
}
