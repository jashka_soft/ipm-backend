<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Upload;
use Illuminate\Http\Request;

/**
 * Class UploadController
 * @package App\Http\Controllers\Admin
 */
class UploadController extends Controller
{
    /**
     * @param Request $request
     * @param Upload $query
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(Request $request, Upload $query)
    {
        if ($request->has('file')) {

            $upload = $query->whereTheoryId($request->get('theory_id'))->first();

            if ($upload !== null) {
                if (\Storage::disk('s3')->exists($upload->path)) {
                    \Storage::disk('s3')->delete($upload->path);
                }
                $upload->delete();
            }

            $upload = $query->create([
                'theory_id' => $request->get('theory_id'),
                'path' => Upload::file($request->get('file'), $request->get('extension')),
            ]);

            return $upload;
        }

        return response()->json([
            'data' => $query->whereTheoryId($request->get('theory_id'))->firstOrFail(),
            'message' => trans('api.uploaded')
        ]);
    }
}
