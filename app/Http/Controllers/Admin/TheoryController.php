<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TheoryRequest;
use Illuminate\Http\Request;
use Repository\TheoryRepository;

/**
 * Class TheoryController
 * @package App\Http\Controllers\Admin
 */
class TheoryController extends Controller
{
    /**
     * @var TheoryRepository
     */
    private $theoryRepository;

    /**
     * TheoryController constructor.
     * @param TheoryRepository $theoryRepository
     */
    public function __construct(TheoryRepository $theoryRepository)
    {
        $this->theoryRepository = $theoryRepository;
    }

    /**
     * @param TheoryRequest|Request $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(TheoryRequest $request)
    {
        $theory = $this->theoryRepository
            ->updateOrCreate(
                $request->only(['iframe', 'unit_id']),
                $request->get('unit_id')
            );

        return response()->json([
            'data' => $theory,
            'message' => trans('api.updated')
        ]);
    }
}
