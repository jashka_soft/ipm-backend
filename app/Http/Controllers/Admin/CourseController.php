<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CourseRequest;
use App\Model\Course;
use Repository\CourseRepository;
use Illuminate\Http\Request;

/**
 * Class CourceController
 * @package App\Http\Controllers\Admin
 */
class CourseController extends Controller
{
    /**
     * @var Course
     */
    private $courseRepository;

    /**
     * CourseController constructor.
     * @param CourseRepository $courseRepository
     */
    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    public function index(Request $request)
    {
        $data = $this->courseRepository->getAllPaginate($request->all());

        return response()->json(compact('data'));
    }

    public function show($id)
    {
        $data = $this->courseRepository->findById($id);

        return response()->json(compact('data'));
    }

    public function update($id, CourseRequest $request)
    {
        $courseData = $request->only(['name', 'description']);
        $course = $this->courseRepository->update($id, $courseData);

        return response()->json([
            'data' => $course,
            'message' => trans('api.updated')
        ]);
    }

    public function store(CourseRequest $request)
    {
        $courseData = $request->only(['name', 'description']);
        $course = $this->courseRepository->create(array_merge($courseData, ['user_id' => auth()->user()->id]));

        return response()->json([
            'data' => $course,
            'message' => trans('api.created')
        ], 201);
    }

    public function restore($id)
    {
        $course = $this->courseRepository->restore((int)$id);

        return response()->json([
            'data' => $course,
            'message' => trans('api.restored')
        ]);
    }

    public function destroy($id)
    {
        $this->courseRepository->delete((int)$id);

        return response()->json([
            'message' => trans('api.deleted')
        ]);
    }
}
