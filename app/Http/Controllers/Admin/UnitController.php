<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UnitRequest;
use App\Model\Unit;
use Illuminate\Http\Request;
use Repository\UnitRepository;

class UnitController extends Controller
{
    /**
     * @var UnitRepository
     */
    private $unitRepository;

    /**
     * UnitController constructor.
     * @param UnitRepository $unitRepository
     */
    public function __construct(UnitRepository $unitRepository)
    {
        $this->unitRepository = $unitRepository;
    }

    public function index(Request $request)
    {
        $data = $this->unitRepository->getUnitsByLessonId((int)$request->get('lesson_id'));
        $data->setPath($request->fullUrl());
        $collection = $data->getCollection()->map(function (Unit $unit) {
           return $unit->append('users_active');
        });

        $data->setCollection($collection);

        return response()->json(compact('data'));
    }

    public function show($id)
    {
        $data = $this->unitRepository->getUnit((int)$id);
        $data->append('users_active');

        return response()->json(compact('data'));
    }

    public function update($id, UnitRequest $request)
    {
        $unitData = $request->only(['name', 'description', 'lesson_id']);
        $unit = $this->unitRepository->updateById($id, $unitData);

        return response()->json([
            'data' => $unit,
            'message' => trans('api.updated')
        ]);
    }

    public function store(UnitRequest $request)
    {
        $unitData = $request->only(['name', 'description', 'lesson_id']);
        $unit = $this->unitRepository->create($unitData);

        return response()->json([
            'data' => $unit,
            'message' => trans('api.created')
        ], 201);
    }

    public function restore($id)
    {
        $unit = $this->unitRepository->restore([ (int)$id ]);

        return response()->json([
            'data' => $unit,
            'message' => trans('api.restored')
        ]);
    }

    public function destroy($id)
    {
        $this->unitRepository->delete([$id]);

        return response()->json([
            'message' => trans('api.deleted')
        ]);
    }
}
