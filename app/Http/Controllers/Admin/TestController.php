<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TestRequest;
use App\Model\Test;

class TestController extends Controller
{
    public function store(TestRequest $request, Test $testQuery)
    {

        if ($request->get('id')) {
            $test = $testQuery->find($request->get('id'));
            $test->update($request->only(['name', 'unit_id']));
        } else {
            $test = $testQuery->create($request->only(['name', 'unit_id']));
        }

        $test->fillQuestions($request->get('questions'));

        return response()->json([
            'data' => $test->with('questions.answers')->first(),
            'message' => trans('api.updated')
        ]);
    }
}
