<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LessonRequest;
use Illuminate\Http\Request;
use Repository\LessonRepository;

class LessonController extends Controller
{
    /**
     * @var LessonRepository
     */
    private $lessonRepository;

    /**
     * LessonController constructor.
     * @param LessonRepository $lessonRepository
     */
    public function __construct(LessonRepository $lessonRepository)
    {
        $this->lessonRepository = $lessonRepository;
    }

    public function index(Request $request)
    {
        $data = $this->lessonRepository->byCourseId($request->get('course_id'))->paginate();
        $data->setPath($request->fullUrl());

        return response()->json(compact('data'));
    }

    public function show($id)
    {
        $data = $this->lessonRepository->getLesson($id);

        return response()->json(compact('data'));
    }

    public function update($id, LessonRequest $request)
    {
        $lessonData = $request->only(['name', 'description']);
        $lesson = $this->lessonRepository->update($id, $lessonData);

        return response()->json([
            'data' => $lesson,
            'message' => trans('api.updated')
        ]);
    }

    public function store(LessonRequest $request)
    {
        $lessonData = $request->only(['name', 'description', 'course_id']);
        $lesson = $this->lessonRepository->create($lessonData);

        return response()->json([
            'data' => $lesson,
            'message' => trans('api.created')
        ], 201);
    }

    public function restore($id)
    {
        $lesson = $this->lessonRepository->restore([$id]);

        return response()->json([
            'data' => $lesson,
            'message' => trans('api.restored')
        ]);
    }

    public function destroy($id)
    {
        $this->lessonRepository->delete([$id]);

        return response()->json([
            'message' => trans('api.deleted')
        ]);
    }
}
