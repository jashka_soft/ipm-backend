<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DefinitionRequest;
use App\Model\Definition;
use Illuminate\Http\Request;
use Repository\DefinitionRepository;

class DefinitionController extends Controller
{
    /**
     * @var DefinitionRepository
     */
    private $definitionRepository;

    /**
     * DefinitionController constructor.
     * @param DefinitionRepository $definitionRepository
     */
    public function __construct(DefinitionRepository $definitionRepository)
    {
        $this->definitionRepository = $definitionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->definitionRepository->getAllPaginate();
        $data->setPath($request->fullUrl());

        return response()->json(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DefinitionRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DefinitionRequest $request)
    {
        $definition = $this->definitionRepository->create($request->all());

        return response()->json([
            'data' => $definition,
            'message' => trans('api.created')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $definition = $this->definitionRepository->findById($id);

        return response()->json([
            'data' => $definition,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DefinitionRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DefinitionRequest $request, $id)
    {
        $definition = $this->definitionRepository->update($id, $request->all());

        return response()->json([
            'data' => $definition,
            'message' => trans('api.updated')
        ]);
    }

    public function restore($id)
    {
        $definition = $this->definitionRepository->restore($id);

        return response()->json([
            'data' => $definition,
            'message' => trans('api.restored')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param Definition $definition
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Definition $definition)
    {
        $definition = $definition->find($id);
        $definition->delete();

        return response()->json([
            'message' => trans('api.deleted')
        ]);
    }
}
