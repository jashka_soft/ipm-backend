<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchemaRequest;
use App\Model\Schema;
use App\Policies\SchemaPolicy;
use Illuminate\Http\Request;

class SchemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Schema::onlyUser(auth()->user()->id)->withTrashed()->paginate();

        return response()->json(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SchemaRequest|Request $request
     * @param Schema $query
     * @return \Illuminate\Http\Response
     */
    public function store(SchemaRequest $request, Schema $query)
    {
        $data = $request->only(['unit_id', 'diagram', 'type', 'name']);

        $data['user_id'] = auth()->user()->id;

        if ($request->has('id')) {
            $schema = $query->whereId($request->get('id'))->first();
            $schema->fill($data);
            $schema->save();
            $message = trans('api.updated');
        } else {
            $schema = $query->create($data);
            $message = trans('api.created');
        }

        return response()->json([
            'data' => $schema,
            'message' => $message
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $data = Schema::onlyUser(auth()->user()->id)->where('id', $id)->firstOrFail();
        if (!$user->can(SchemaPolicy::CAN_VIEW, $data)) {
            return response()->json([
                'message' => trans('api.access_denied')
            ], 403);
        }

        return response()->json(compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schema = Schema::withTrashed()->find($id);
        $user = auth()->user();
        if (!$user->can(SchemaPolicy::CAN_DELETE, $schema)) {
            return response()->json([
                'message' => trans('api.access_denied')
            ], 403);
        }

        $schema->forceDelete();

        return response()->json([
            'message' => trans('api.deleted')
        ]);
    }
}
