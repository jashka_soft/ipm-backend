<?php

namespace App\Http\Middleware;


use Closure;
use Tymon\JWTAuth\JWTAuth;

class Role
{
    /**
     * @var JWTAuth
     */
    private $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $this->jwt->parseToken()->authenticate();

        if ($user->role !== $role) {
            return response()->json([
                'message' => 'Access denied',
                'data' => null
            ], 401);
        }

        return $next($request);
    }
}
