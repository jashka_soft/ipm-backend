<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'questions.*.name' => 'required',
            'questions.*.answer_id' => 'required',
            'questions.*.answers' => 'required',
            'questions.*.answers.*.name' => 'required',
            'unit_id' => 'required'
        ];
    }

}
