<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 19.10.17
 * Time: 19:22
 */

namespace Repository;


use App\Model\Test;

class TestRepository
{
    /**
     * @var Test
     */
    private $model;
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * TestRepository constructor.
     * @param Test $testQuery
     * @param QuestionRepository $questionRepository
     */
    public function __construct(
        Test $testQuery,
        QuestionRepository $questionRepository
    )
    {
        $this->model = $testQuery;
        $this->questionRepository = $questionRepository;
    }

    public function getIdsByUnit(array $unitIds)
    {
        return $this->model
            ->withTrashed()
            ->whereIn('unit_id', $unitIds)
            ->pluck('id')
            ->toArray();
    }

    public function restoreByUnit(array $unitIds)
    {
        $ids = $this->getIdsByUnit($unitIds);

        $this->model->whereIn('unit_id', $ids)->restore();
        $this->questionRepository->restoreByTest($ids);
    }

    public function softDeleteByUnit(array $unitIds)
    {
        $ids = $this->getIdsByUnit($unitIds);

        $this->delete($ids);
    }

    public function delete(array $ids)
    {
        $this->questionRepository->softDeleteByTest($ids);
        $this->model->destroy($ids);
    }
}