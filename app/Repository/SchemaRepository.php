<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 19.10.17
 * Time: 19:21
 */

namespace Repository;


use App\Model\Schema;

/**
 * Class SchemaRepository
 * @package Repository
 */
class SchemaRepository
{

    /**
     * @var Schema
     */
    private $model;

    /**
     * SchemaRepository constructor.
     * @param Schema $schemaQuery
     */
    public function __construct(Schema $schemaQuery)
    {
        $this->model = $schemaQuery;
    }

    /**
     * @param $unitId
     * @return mixed|Schema
     */
    public function whereUnit($unitId)
    {
        return $this->model
            ->where('unit_id', $unitId)
            ->withTrashed()
            ->first();
    }

    public function restoreByUnit(array $unitIds)
    {
        $this->model
            ->withTrashed()
            ->whereIn('unit_id', $unitIds)->restore();
    }

    public function softDeleteByUnit(array $unitIds)
    {
        $this->model
            ->whereIn('unit_id', $unitIds)
            ->delete();
    }
}