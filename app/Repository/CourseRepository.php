<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 23.12.17
 * Time: 22:05
 */

namespace Repository;


use App\Model\Course;
use Illuminate\Database\Eloquent\Builder;

class CourseRepository
{
    /**
     * @var Course
     */
    private $model;
    /**
     * @var LessonRepository
     */
    private $lessonRepository;

    /**
     * CourseRepository constructor.
     * @param Course $courseQuery
     * @param LessonRepository $lessonRepository
     */
    public function __construct(
        Course $courseQuery,
        LessonRepository $lessonRepository
    )
    {
        $this->model = $courseQuery;
        $this->lessonRepository = $lessonRepository;
    }

    public function getAllPaginate(array $params = [])
    {
        return $this->model
            ->orderByDeleted()
            ->withTrashed()
            ->paginate();
    }

    protected function filterByType (Builder $query) {
        switch ($query) {
            case 1:
                $query = $query->onlyTrashed();
                break;

            case 0:
                $query = $query->withTrashed();
                break;
        }

        return $query;
    }

    protected function filterBySearch (Builder $query, $search) {
        $query = $query
            ->where('name', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%');

        return $query;
    }

    protected function getSearch($params)
    {
        return array_get($params, 'search');
    }

    protected function getType($params)
    {
        return array_get($params, 'type');
    }

    public function findById(int $id)
    {
        return $this->model
            ->where('id', '=', $id)
            ->withTrashed()
            ->first();
    }

    public function update($id, $courseData)
    {
        $course = $this->findById($id);
        $course->fill($courseData);
        $course->save();

        return $course;
    }

    public function create($course)
    {
        return $this->model->create($course);
    }

    public function restore(int $id)
    {
        $course = $this->findById($id);

        \DB::transaction(function () use ($course) {
            $this->lessonRepository->restoreByCourse($course->id);
            $course->restore();
        });

        return $course;
    }

    public function delete(int $id)
    {
        $course = $this->findById($id);

        \DB::transaction(function () use ($course) {
            $this->lessonRepository->softDeleteByCourse($course->id);
            $course->delete();
        });

        return $course;
    }

}