<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 14.01.18
 * Time: 16:08
 */

namespace Repository;


use App\Model\Definition;

class DefinitionRepository
{
    /**
     * @var Definition
     */
    private $model;

    public function __construct(Definition $definition)
    {
        $this->model = $definition;
    }

    public function getAllPaginate()
    {
        return $this->model
            ->orderByDeleted()
            ->withTrashed()
            ->paginate();
    }

    public function findById(int $id)
    {
        return $this->model
            ->where('id', '=', $id)
            ->withTrashed()
            ->first();
    }

    public function update($id, $definitionData)
    {
        $definition = $this->findById($id);
        $definition->fill($definitionData);
        $definition->save();

        return $definition;
    }

    public function create($definition)
    {
        return $this->model->create($definition);
    }

    public function restore(int $id)
    {
        $definition = $this->model
            ->onlyTrashed()
            ->where('id', '=', $id)
            ->first();

        $definition->restore();

        return $definition;
    }

    public function delete(int $id)
    {
        $definition = $this->findById($id);

        $definition->delete();

        return $definition;
    }
}