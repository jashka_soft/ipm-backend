<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 08.10.17
 * Time: 22:35
 */

namespace Repository;


use App\Model\ExaminationInfo;
use App\Model\SchemaResult;
use App\Model\TestResult;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Services\UUID;

/**
 * Class ExaminationRepository
 * @package Repository
 */
class ExaminationInfoRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        return ExaminationInfo::query();
    }

    /**
     * @param string $uuid
     */
    public function cancel(string $uuid)
    {
        \DB::transaction(function () use ($uuid) {
            $examination = $this->getActiveExamination($uuid);

            $schema = SchemaResult::whereExaminationInfoId($examination->id)->first();
            if ($schema) {
                $schema->forceDelete();
            }
            $questions = TestResult::whereExaminationInfoId($examination->id)->get();
            foreach ($questions as $question) {
                $question->forceDelete();
            }

            $examination->forceDelete();
        });
    }

    public function removeExpired()
    {
        $uuids = [];
        $examinations = $this->getQuery()
            ->withTrashed()
            ->started()
            ->get();

        foreach ($examinations as $examination) {
            $minutes = $examination->created_at->diffInMinutes(Carbon::now());
            $isExpired = $minutes > ExaminationInfo::MAX_MINUTES;
            if ($isExpired) {
                $uuids[] = $examination->uuid;
                $this->cancel($examination->uuid);
            }
        }

        return $uuids;
    }

    /**
     * @param int $userId
     * @param int $unitId
     * @return int
     */
    public function getCountExaminations(int $userId, int $unitId): int
    {
        return $this
            ->getQuery()
            ->where('user_id', $userId)
            ->where('unit_id', $unitId)
            ->count();
    }

    public function getActiveExaminations()
    {
        return $this->getQuery()
            ->with([ 'unit' ])
            ->where('user_id', auth()->user()->id)
            ->where('status', ExaminationInfo::STARTED)
            ->get();
    }

    /**
     * @param string $uuid
     * @return ExaminationInfo | null
     */
    public function examination(string $uuid)
    {
        return $this
            ->getQuery()
            ->where('uuid', $uuid)
            ->first();
    }

    /**
     * @param int $unitId
     * @param int $userId
     * @return Model|ExaminationInfo
     */
    public function create(int $unitId, int $userId)
    {
        $attrs = [
            'user_id' => $userId,
            'unit_id' => $unitId,
            'status' => ExaminationInfo::STARTED,
        ];

        $examination = $this
            ->getQuery()
            ->create(array_merge($attrs, [
                    'uuid' => UUID::v4(),
                    'current_tab' => ExaminationInfo::THEORY
                ])
            );

        return $examination;
    }

    /**
     * @param string $uuid
     * @return mixed
     * @internal param int $userId
     * @internal param int $unitId
     */
    public function getActiveExamination(string $uuid)
    {
        $ex = $this
            ->getQuery()
            ->where('uuid', $uuid)
            ->where('user_id', auth()->user()->id)
            ->started()
            ->first();

        return $ex;
    }

    /**
     * @param $model
     * @param int $status
     * @return mixed
     */
    public function updateStatus(ExaminationInfo $model, int $status)
    {
        $model->fill([
            'status' => $status
        ]);

        $model->save();

        return $model;
    }

    /**
     * @param ExaminationInfo $model
     * @param int $tab
     * @return mixed
     * @internal param string $uuid
     * @internal param int $status
     */
    public function updateTab(ExaminationInfo $model, int $tab)
    {
        $model->fill([
            'current_tab' => $tab
        ]);

        $model->save();

        return $model;

    }
}