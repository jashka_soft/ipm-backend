<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 23.12.17
 * Time: 22:16
 */

namespace Repository;


use App\Model\Lesson;

class LessonRepository
{
    /**
     * @var Lesson
     */
    private $model;
    /**
     * @var UnitRepository
     */
    private $unitRepository;

    /**
     * LessonRepository constructor.
     * @param Lesson $lesson
     * @param UnitRepository $unitRepository
     */
    public function __construct(
        Lesson $lesson,
        UnitRepository $unitRepository
    )
    {
        $this->model = $lesson;
        $this->unitRepository = $unitRepository;
    }

    public function byCourseId(int $courseId)
    {
        return $this->model
            ->where('course_id', '=', $courseId)
            ->withTrashed();
    }

    public function getLesson(int $id)
    {
        return $this->model
            ->orderByDeleted()
            ->withTrashed()
            ->where('id', '=', $id)->first();
    }

    public function update(int $id, $lessonData)
    {
        $lesson = $this->model
            ->withTrashed()
            ->find($id);
        $lesson->fill($lessonData);
        $lesson->save();

        return $lesson;
    }

    public function create($lessonData)
    {
        $lesson = $this->model->create($lessonData);

        return $lesson;
    }

    protected function getIdsByCourse($courseId)
    {
        return $this->model
            ->withTrashed()
            ->whereIn('course_id', [$courseId])
            ->pluck('id')
            ->toArray();
    }

    public function restoreByCourse($courseId)
    {
        $ids = $this->getIdsByCourse($courseId);

        $this->restore($ids);
    }

    public function restore (array $ids)
    {
        $this->model
            ->withTrashed()
            ->whereIn('id', $ids)
            ->restore();
        $this->unitRepository->restoreByLesson($ids);

        return $ids;
    }

    public function softDeleteByCourse(int $courseId)
    {
        $ids = $this->getIdsByCourse($courseId);

        $this->delete($ids);
    }

    public function delete(array $ids)
    {
        $this->unitRepository->softDeleteByLessons($ids);
        $this->model->destroy($ids);
    }

}