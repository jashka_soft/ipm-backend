<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 12.01.18
 * Time: 15:13
 */

namespace Repository;

use App\Model\Behavior\GetIds;
use App\Model\Theory;
use App\Model\Upload;

/**
 * Class TheoryRepository
 * @package Repository
 */
class TheoryRepository
{
    use GetIds;

    /**
     * @var Theory
     */
    private $model;
    /**
     * @var Upload
     */
    private $uploadQuery;

    /**
     * TheoryRepository constructor.
     * @param Theory $theoryQuery
     * @param Upload $uploadQuery
     */
    public function __construct(Theory $theoryQuery, Upload $uploadQuery)
    {
        $this->model = $theoryQuery;
        $this->uploadQuery = $uploadQuery;
    }

    public function updateOrCreate(array $data, int $unitId)
    {
        $theory = $this->model
            ->withTrashed()
            ->where('unit_id', '=', $unitId)
            ->first();

        if (!$theory) {
            $theory = new Theory();
        }

        $theory->fill($data);
        $theory->save();

        return $theory;
    }

    public function restoreByUnit(array $unitIds)
    {
        $ids = $this->getIds($unitIds, 'unit_id');

        $this->uploadQuery->whereIn('theory_id', $ids)->restore();
        $this->model->whereIn('unit_id', $unitIds)->restore();
    }

    public function softDeleteByUnit(array $unitIds)
    {
        $ids = $this->getIds($unitIds, 'unit_id');

        $this->delete($ids);
    }

    public function delete(array $ids)
    {
        $this->uploadQuery->whereIn('theory_id', $ids)->delete();
        $this->model->destroy($ids);
    }
}