<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 26.12.17
 * Time: 18:42
 */

namespace Repository;


use App\Model\Unit;

class UnitRepository
{
    /**
     * @var Unit
     */
    private $model;
    /**
     * @var TestRepository
     */
    private $testRepository;
    /**
     * @var TheoryRepository
     */
    private $theoryRepository;
    /**
     * @var SchemaRepository
     */
    private $schemaRepository;
    /**
     * @var ExaminationInfoRepository
     */
    private $examinationInfoRepository;

    /**
     * UnitRepository constructor.
     * @param Unit $unit
     * @param TestRepository $testRepository
     * @param TheoryRepository $theoryRepository
     * @param SchemaRepository $schemaRepository
     * @param ExaminationInfoRepository $examinationInfoRepository
     */
    public function __construct(
        Unit $unit,
        TestRepository $testRepository,
        TheoryRepository $theoryRepository,
        SchemaRepository $schemaRepository,
        ExaminationInfoRepository $examinationInfoRepository
    )
    {
        $this->model = $unit;
        $this->testRepository = $testRepository;
        $this->theoryRepository = $theoryRepository;
        $this->schemaRepository = $schemaRepository;
        $this->examinationInfoRepository = $examinationInfoRepository;
    }

    public function getUnitsByLessonId(int $id)
    {
        return $this->model
            ->orderByDeleted()
            ->withTrashed()
            ->where('lesson_id', '=', $id)
            ->paginate();
    }

    public function getUnit(int $id)
    {
        return $this->model
            ->where('id', '=', $id)
            ->withTrashed()
            ->with([
                'theory' => function ($builder) {
                    return $builder->withTrashed();
                },
                'test.questions.answers' => function ($builder) {
                    return $builder->withTrashed();
                },
                'schema' => function ($builder) {
                    return $builder->withTrashed();
                }
            ])
            ->first();
    }

    public function updateById(int $id, $unitData)
    {
        $unit = $this->model
            ->whereId($id)
            ->withTrashed()
            ->first();
        $unit->fill($unitData);
        $unit->save();

        return $unit;
    }

    public function create($unitData)
    {
        $unit = $this->model->create($unitData);

        return $unit;
    }

    protected function getIdsByLesson($lessonIds, $foreignColumn = '', $column = 'id')
    {
        return $this->model
            ->select(['id'])
            ->withTrashed()
            ->whereIn('lesson_id', $lessonIds)
            ->pluck('id')
            ->toArray();
    }

    public function restoreByLesson($lessonIds)
    {
        $ids = $this->getIdsByLesson($lessonIds);

        $this->restore($ids);
    }

    public function restore (array $ids)
    {
        $this->model->whereIn('id', $ids)->withTrashed()->restore();

        $this->theoryRepository->restoreByUnit($ids);
        $this->schemaRepository->restoreByUnit($ids);
        $this->testRepository->restoreByUnit($ids);

        return $ids;
    }

    public function softDeleteByLessons($lessonIds)
    {
        $ids = $this->getIdsByLesson($lessonIds);

        $this->delete($ids);
    }

    public function delete(array $ids)
    {
        $this->theoryRepository->softDeleteByUnit($ids);
        $this->schemaRepository->softDeleteByUnit($ids);
        $this->testRepository->softDeleteByUnit($ids);
        $this->model->destroy($ids);
    }
}