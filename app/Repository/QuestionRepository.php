<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 12.01.18
 * Time: 18:20
 */

namespace Repository;


use App\Model\Question;

class QuestionRepository
{

    /**
     * @var Question
     */
    private $model;
    /**
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * TestRepository constructor.
     * @param Question $questionQuery
     * @param AnswerRepository $answerRepository
     */
    public function __construct(
        Question $questionQuery,
        AnswerRepository $answerRepository
    )
    {
        $this->model = $questionQuery;
        $this->answerRepository = $answerRepository;
    }

    public function getIdsByTest(array $testIds)
    {
        return $this->model
            ->withTrashed()
            ->whereIn('test_id', $testIds)
            ->pluck('id')
            ->toArray();
    }

    public function restoreByTest(array $testIds)
    {
        $ids = $this->getIdsByTest($testIds);

        $this->answerRepository->restoreByQuestion($ids);
        $this->model->whereIn('test_id', $ids)->restore();
    }

    public function softDeleteByTest($testIds)
    {
        $ids = $this->getIdsByTest($testIds);

        $this->delete($ids);
    }

    public function delete(array $ids)
    {
        $this->answerRepository->softDeleteByQuestion($ids);
        $this->model->destroy($ids);
    }
}