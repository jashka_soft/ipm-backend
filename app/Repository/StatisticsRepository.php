<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 08.10.17
 * Time: 22:35
 */

namespace Repository;


use App\Model\ExaminationInfo;
use App\Model\Schema;
use App\Model\SchemaResult;
use App\Model\TestResult;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Services\UUID;

/**
 * Class ExaminationRepository
 * @package Repository
 */
class StatisticsRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        return ExaminationInfo::query();
    }

    /**
     * @param array $params
     * @param $userData
     * @internal param $user
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(array $params, $userData)
    {
        $query = $this->getQuery();
        $status = $this->getStatus($params);
        $tab = $this->getTab($params);
        $user = $this->getUser($params);
        $role = $this->getRole($userData);
        $unit = $this->getUnit($params);
        $search = $this->getSearch($params);

        if ($role === 'user') {
            $query = $this->filterByUser($query, $userData->id);
        }

        if ($role === 'admin') {
            $query = $this->filterByUser($query, $user);
            $query = $query->withTrashed();
        }

        $query->with([
            'user',
            'unit'
        ]);

        $query = $this->filterByStatus($query, $status);
        $query = $this->filterByTab($query, $tab);
        $query = $this->filterByUnit($query, $unit);
        $query = $this->filterBySearch($query, $search);

        $query = $query->latest();

        $data = $query->paginate();

        return $data;

    }

    /**
     * @param $id
     * @param $userData
     * @return mixed
     */
    public function loadResult($id, $userData)
    {
        $query = $this->getQuery();
        $role = $this->getRole($userData);

        if ($role === 'user') {
            $query = $this->filterByUser($query, $userData->id);
        }

        $result = $query
            ->where('id', '=', $id)
            ->with([
                'user',
                'unit.theory',
                'schema',
                'test.correct_answer',
                'test.answer',
                'test.question',
            ])
            ->first();

        $result->schema->append('type');

        return $result;

    }

    public function softDelete(int $id)
    {
        \DB::transaction(function () use ($id) {
            $examination = $this
                ->getQuery()
                ->with(['schema', 'test'])
                ->find($id);

            $schemaResult = $examination->schema;
            $schemaResult->delete();

            $testResults = $examination->test;
            foreach ($testResults as $result) {
                $result->delete();
            }

            $examination->delete();
        });
    }

    public function restore(int $id)
    {

    }

    /**
     * @param Builder $query
     * @param $unit
     * @return Builder
     */
    protected function filterByUnit(Builder $query, $unit)
    {
        if ($unit) {
            $query->where('unit_id', '=', $unit);
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $userId
     * @return Builder
     */
    protected function filterByUser(Builder $query, $userId)
    {
        if ($userId) {
            $query->where('user_id', '=', $userId);
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $tab
     * @return Builder
     */
    protected function filterByTab(Builder $query, $tab)
    {
        if ($tab) {
            $query->tab(intval($tab));
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $status
     * @return Builder
     */
    protected function filterByStatus(Builder $query, $status)
    {
        // $query->where('status', '>', ExaminationInfo::STARTED);
        if ($status) {
            $query->where('status', '=', $status);
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $search
     * @return Builder
     */
    protected function filterBySearch(Builder $query, $search)
    {
        if ($search) {
            $query->whereHas('unit', function (Builder $query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
                ->orWhereHas('user', function (Builder $query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                });
        }

        return $query;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function getSearch(array $params)
    {
        return array_get($params, 'search');
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function getUnit($params)
    {
        return array_get($params, 'unit_id');
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function getStatus($params)
    {
        return array_get($params, 'status');
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function getTab($params)
    {
        return array_get($params, 'current_tab');
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function getUser($params)
    {
        return array_get($params, 'user_id');
    }

    /**
     * @param $user
     * @return array
     */
    protected function getRole($user)
    {
        return $user->role;
    }
}