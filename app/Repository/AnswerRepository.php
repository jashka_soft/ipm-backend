<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 12.01.18
 * Time: 18:21
 */

namespace Repository;


use App\Model\Answer;

class AnswerRepository
{
    /**
     * @var Answer
     */
    private $model;

    /**
     * AnswerRepository constructor.
     * @param Answer $answerQuery
     */
    public function __construct(Answer $answerQuery)
    {
        $this->model = $answerQuery;
    }

    public function getIdsByQuestion(array $questionIds)
    {
        return $this->model
            ->withTrashed()
            ->whereIn('question_id', $questionIds)
            ->pluck('id')
            ->toArray();
    }

    public function restoreByQuestion(array $questionIds)
    {
        $ids = $this->getIdsByQuestion($questionIds);
        $this->model->whereIn('question_id', $ids)->restore();
    }

    public function softDeleteByQuestion($questionIds)
    {
        $ids = $this->getIdsByQuestion($questionIds);

        $this->delete($ids);
    }

    public function delete(array $ids)
    {
        $this->model->destroy($ids);
    }
}