<?php

namespace App\Console\Commands;

use function foo\func;
use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\MySqlBuilder;

class DatabaseRelationsCommand extends Command
{
    const UP = 'up';
    const DOWN = 'down';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:rl {type : up or down}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Toggle relations on database';

    /**
     * @var MySqlBuilder
     */
    private $schema;

    /**
     * Create a new command instance.
     * @param MySqlBuilder $schema
     */
    public function __construct(MySqlBuilder $schema)
    {
        parent::__construct();

        $this->schema = $schema;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        switch ($type) {
            case self::UP:
                $this->up();
                break;

            case self::DOWN:
                $this->down();
                break;

            default:
                $this->error('Type "' . $type . '" not declared' . PHP_EOL . 'Use "up" or "down"');
        }
    }

    protected function up()
    {
        \DB::transaction(function () {
            // course
            $this->schema->table('courses', function (Blueprint $table) {
                $table->integer('user_id')->unsigned()->change();
                $table->foreign('user_id')->references('id')->on('users');
            });

            $this->schema->table('lessons', function (Blueprint $table) {
                $table->integer('course_id')->unsigned()->change();
                $table->foreign('course_id')->references('id')->on('courses');
            });

            // unit
            $this->schema->table('units', function (Blueprint $table) {
                $table->integer('lesson_id')->unsigned()->change();
                $table->foreign('lesson_id')->references('id')->on('lessons');
            });

            $this->schema->table('theories', function (Blueprint $table) {
                $table->integer('unit_id')->unsigned()->change();
                $table->foreign('unit_id')->references('id')->on('units');
            });

            $this->schema->table('uploads', function (Blueprint $table) {
                $table->integer('theory_id')->unsigned()->change();
                $table->foreign('theory_id')->references('id')->on('theories');
            });

            $this->schema->table('schemas', function (Blueprint $table) {
                $table->integer('unit_id')->unsigned()->change();
                $table->foreign('unit_id')->references('id')->on('units');
            });

            // tests
            $this->schema->table('tests', function (Blueprint $table) {
                $table->integer('unit_id')->unsigned()->change();
                $table->foreign('unit_id')->references('id')->on('units');
            });

            $this->schema->table('questions', function (Blueprint $table) {
                $table->foreign('test_id')->references('id')->on('tests');
                $table->foreign('answer_id')->references('id')->on('answers');
            });

            $this->schema->table('answers', function (Blueprint $table) {
                $table->integer('question_id')->unsigned()->change();
                $table->foreign('question_id')->references('id')->on('questions');
            });

            // examination
            $this->schema->table('examination_infos', function (Blueprint $table) {
                $table->integer('user_id')->unsigned()->change();
                $table->integer('unit_id')->unsigned()->change();
                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('unit_id')->references('id')->on('units');
            });

            $this->schema->table('schema_results', function (Blueprint $table) {
                $table->foreign('schema_id')->references('id')->on('schemas');
                $table->foreign('examination_info_id')->references('id')->on('examination_infos');
            });

            $this->schema->table('test_results', function (Blueprint $table) {
                $table->foreign('question_id')->references('id')->on('questions');
                $table->foreign('examination_info_id')->references('id')->on('examination_infos');
                $table->foreign('answer_id')->references('id')->on('answers');
                $table->foreign('correct_answer_id')->references('id')->on('answers');
            });
        });
    }

    protected function down()
    {
        \DB::transaction(function () {
            $this->schema->table('test_results', function (Blueprint $table) {
                $table->dropForeign(['question_id']);
                $table->dropForeign(['examination_info_id']);
                $table->dropForeign(['answer_id']);
                $table->dropForeign(['correct_answer_id']);
            });
            $this->schema->table('schema_results', function (Blueprint $table) {
                $table->dropForeign(['schema_id']);
                $table->dropForeign(['examination_info_id']);
            });
            // examination
            $this->schema->table('examination_infos', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropForeign(['unit_id']);
            });
            $this->schema->table('answers', function (Blueprint $table) {
                $table->dropForeign(['question_id']);
            });
            $this->schema->table('questions', function (Blueprint $table) {
                $table->dropForeign(['test_id']);
                $table->dropForeign(['answer_id']);
            });
            // tests
            $this->schema->table('tests', function (Blueprint $table) {
                $table->dropForeign(['unit_id']);
            });
            $this->schema->table('schemas', function (Blueprint $table) {
                $table->dropForeign(['unit_id']);
            });
            $this->schema->table('uploads', function (Blueprint $table) {
                $table->dropForeign(['theory_id']);
            });
            $this->schema->table('theories', function (Blueprint $table) {
                $table->dropForeign(['unit_id']);
            });
            // unit
            $this->schema->table('units', function (Blueprint $table) {
                $table->dropForeign(['lesson_id']);
            });
            $this->schema->table('lessons', function (Blueprint $table) {
                $table->dropForeign(['course_id']);
            });

            // course
            $this->schema->table('courses', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
            });

        });
    }
}
