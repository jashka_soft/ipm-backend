<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 01.07.17
 * Time: 21:08
 */

namespace App\Observer;


use App\Model\Upload;
use Illuminate\Filesystem\Filesystem;

/**
 * Class UploadObserver
 * @package App\Observer
 */
class UploadObserver
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * UploadObserver constructor.
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param Upload $upload
     */
    public function deleted(Upload $upload)
    {
        $this->removeIfExists($upload->path);
    }

    /**
     * @param Upload $upload
     */
    public function updating(Upload $upload)
    {
        $this->removeIfExists($upload->path);
    }

    /**
     * @param $path
     */
    protected function removeIfExists($path)
    {
        $path = public_path($path);

        if ($this->filesystem->exists($path)) {
            $this->filesystem->delete($path);
        }
    }
}