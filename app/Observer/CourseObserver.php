<?php
/**
 * Created by PhpStorm.
 * User: jashka
 * Date: 22.12.17
 * Time: 10:01
 */

namespace App\Observer;


use App\Model\Course;

class CourseObserver
{
    /**
     * @param Course $course
     */
    public function deleting(Course $course)
    {
        $lessons = $course->lessons();

        foreach ($lessons as /** @var \App\Model\Lesson $lesson */
                 $lesson) {
            $lesson->delete();
        }
    }
}