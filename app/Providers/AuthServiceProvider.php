<?php

namespace App\Providers;

use App\Model\ExaminationInfo;
use App\Model\Schema;
use App\Policies\SchemaPolicy;
use App\Policies\StatisticPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Schema::class => SchemaPolicy::class,
        ExaminationInfo::class => StatisticPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
